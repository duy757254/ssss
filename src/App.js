import logo from "./logo.svg";
import "./App.css";
import Baitap2 from "./Bai_Tap_React_2/Baitap2";
import Header from "./Bai_Tap_React_2/Header";

function App() {
  return (
    <div className="App">
      <Header />
      <Baitap2 />
    </div>
  );
}

export default App;
