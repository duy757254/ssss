import React, { Component } from "react";

export default class ItemGlasses extends Component {
  render() {
    let { url, name, price } = this.props.data;

    return (
      <div className="col-4">
        <div className="card text-left mb-3">
          <img
            onClick={() => {
              this.props.changeGlasses(this.props.data);
            }}
            className=" card-img-top"
            src={url}
            alt
          />
          <div className="card-body">
            <h4 className="card-title text-center">{name}</h4>
            <p className="card-text text-center">Giá: {price}$</p>
          </div>
        </div>
      </div>
    );
  }
}
