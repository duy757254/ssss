import React, { Component } from "react";
import "./Total.css";
export default class Header extends Component {
  render() {
    return (
      <header>
        <div className="shadow-lg p-3 mb-5 bg-dark ">
          <div className="container text-center">
            <h1 className="text-primary">
              <h2>Glasses Shop Online</h2>
            </h1>
          </div>
        </div>
      </header>
    );
  }
}
