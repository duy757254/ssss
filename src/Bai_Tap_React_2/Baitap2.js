import React, { Component } from "react";
import { dataGlasses } from "./dataGlasses";
import ItemGlasses from "./ItemGlasses";
import ModelGlasses from "./ModelGlasses";
import "./Total.css";

export default class Baitap2 extends Component {
  state = {
    glassesArr: dataGlasses,
    position: dataGlasses,
  };
  renderListGlasses = () => {
    return this.state.glassesArr.map((item, index) => {
      return (
        <ItemGlasses
          changeGlasses={this.handleChangeGlasses}
          data={item}
          key={index}
        />
      );
    });
  };
  handleChangeGlasses = (glass) => {
    this.setState({
      position: glass,
    });
  };
  render() {
    return (
      <div className="container container_contain d-flex col-12 ">
        <div className="col-4 ">
          <ModelGlasses glassPos={this.state.position} />
        </div>
        <div className="col-8 row p-5">{this.renderListGlasses()}</div>
      </div>
    );
  }
}
